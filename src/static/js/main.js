new WOW().init();

$(document).ready(function () {

    $('.col-2.dots').css('height', $('.dep-item-row').height());

    $('.bxslider').bxSlider({
        auto: true,
        autoControls: true,
        stopAutoOnClick: true,
        pager: true,
        slideWidth: 237,
        slideHeight: 423,
        autoDirection: false
    });

    $('.bx-wrapper').css({
        'max-width': '237px',
        'max-height': '423px'
    });


    $('.bx-prev').html('');
    $('.bx-next').html('');
    $('.bx-start').html('');
    $('.bx-stop').html('');
    $('.bx-pager-link').html('');
    $('.bx-pager-link.active').html('');

    var srHeight = $('.special-recipes').height();
    $('.s-r-bg').css('height', srHeight);
});