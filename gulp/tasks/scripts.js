module.exports = function () {
    $.gulp.task('scripts:lib', function () {
        return $.gulp.src(['node_modules/jquery/dist/jquery.min.js',
            'libs/bxslider/jquery.bxslider.min.js',
            'libs/wow/wow.min.js'])
            .pipe($.gp.concat("libs.min.js"))
            .pipe($.gulp.dest('build/static/js'))
            .pipe($.bs.reload({ //обновление страницы в редактируемом месте
                stream: true
            }));
    });

    $.gulp.task('scripts', function () {
        return $.gulp.src('src/static/js/main.js')
            .pipe($.gulp.dest('build/static/js'))
            .pipe($.bs.reload({ //обновление страницы в редактируемом месте
                stream: true
            }));
    });
}