module.exports = function () {
    $.gulp.task('sass', function () {
        return $.gulp.src('src/static/sass/main.sass')
            .pipe($.gp.sourcemaps.init())
            .pipe($.gp.sass({
                'include css': true
            }))
            .pipe($.gp.autoprefixer({
                browsers: ['last 10 versions'] //здесь можно писать условия для конкретных браузеров
            }))
            .on("error", $.gp.notify.onError({
                title: "Error in style"
            }))
            .pipe($.gp.csso())
            .pipe($.gp.sourcemaps.write())
            .pipe($.gulp.dest('build/static/css'))
            .pipe($.bs.reload({ //обновление страницы в редактируемом месте
                stream: true
            }));
    });
}